export const SIGN_UP = "/signup"
export const SIGN_IN = "/"
export const PROJECT_MANAGEMENT = "/projectmanagement"
export const PROJECT_DASHBOARD = "/projectdashboard"
export const CREATE_PROJECT = "/createproject"
export const NOT_FOUND_PAGE = "/*"

export const USER_INFOR = "userInfor"


/*Notification constant*/
export const NOTIFICATION_SUCCESS = "success"
export const NOTIFICATION_ERROR = "error"
export const NOTIFICATION_WARNING = "warning"

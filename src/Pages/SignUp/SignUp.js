import { Button, Input, message } from 'antd';
import React from 'react'
import Lottie from "lottie-react";
import { useFormik, withFormik } from 'formik';
import { userService } from '../../services/userService';
import * as Yup from 'yup';
import bg_animation from '../../assets/animation-of-ball.json'
import { useNavigate } from 'react-router-dom';
import { SIGN_IN } from '../../utils/constant/constant';


export default function SignUp() {
    const navigate = useNavigate()
    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
            name: "",
            phone: "",
        },
        validationSchema: Yup.object().shape({
            email: Yup.string().required("Email is required").email("email is invalid"),
            password: Yup.string().required("password is required").min(3, "password must have min 6 characters").max(32, "password must have max 32 characters "),
            name: Yup.string().min(6, "Mininum 6 characters").max(15, "Maximum 15 characters").required("Name is Required!"),
        }),


        onSubmit: (values) => {
            userService.signUp(values).then((res) => {
                message.success("sign in successfully")
                setTimeout(() => { navigate(`${SIGN_IN}`) }, 1000)
            }).catch(err => {
                console.log(err);
                message.error(err.response.data.message)
            })
        },
    })
    return (
        <div className='flex'>
            <div className='w-[40%]'>
                <Lottie animationData={bg_animation} className="h-full w-full" />
            </div>
            <div className='w-[60%] my-auto flex flex-col items-start sm:pl-20 md:pl-40 lg:pl-80'>
                <h1 className='text-center mb-10 text-4xl font-bold'>Sign Up Page</h1>
                <form className='mb-10 w-[50%] text-left' onSubmit={formik.handleSubmit}>
                    <div>
                        <Input
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            type="email"
                            placeholder='email'
                            name="email" className='w-full min-w-[300px]' />
                        {formik.errors.email && formik.touched.email ? <span className='text-red-600'>{formik.errors.email}</span> : <></>}
                    </div>

                    <div>
                        <Input
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            type="password"
                            name="password"
                            placeholder='password' className='w-full min-w-[300px] mt-5' />
                        {formik.errors.password && formik.touched.password ? <span className='text-red-600'>{formik.errors.password}</span> : <></>}
                    </div>

                    <div>
                        <Input
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            type="text"
                            name="name"
                            placeholder='name' className='w-full min-w-[300px] mt-5' />
                        {formik.errors.name && formik.touched.name ? <span className='text-red-600'>{formik.errors.name}</span> : <></>}
                    </div>
                    <div>
                        <Input
                            value={formik.values.phone}
                            onChange={formik.handleChange}
                            type="text"
                            name="phone"
                            placeholder='phoneNumber' className='w-full min-w-[300px] mt-5' />
                    </div>
                    <div className='mt-5'>
                        <button type='submit' className='px-3 py-1 border-2 rounded-md'>Sign Up</button>
                    </div>
                </form>
                <div className='flex justify-center text-4xl space-x-3'>

                </div>
            </div>
        </div >
    );
}


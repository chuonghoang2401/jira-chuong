import Lottie from "lottie-react";
import React from 'react'
import bg_notFound from "../../assets/page-not-found.json"

export default function NotFoundPage() {
    return (
        <div className="w-screen h-screen">
            <Lottie animationData={bg_notFound} className="h-full w-full" />
        </div>
    )
}

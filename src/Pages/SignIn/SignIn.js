import { Input, message } from 'antd';
import React from 'react'
import Lottie from "lottie-react";
import { useFormik } from 'formik';
import { userService } from '../../services/userService';
import * as Yup from 'yup';
import bg_animation from '../../assets/animation-of-ball.json'
import { useNavigate } from 'react-router-dom';
import { PROJECT_DASHBOARD, PROJECT_MANAGEMENT, SIGN_UP } from '../../utils/constant/constant';
import { useDispatch } from 'react-redux';
import { setUserInfor } from '../../redux/userReducer/userReducer';
import { FaFacebookSquare, FaGithubSquare, FaGitlab, FaGooglePlusSquare } from "react-icons/fa";
import { localStorageService } from '../../services/localStorageService';

export default function SignIn() {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",

        },
        validationSchema: Yup.object().shape({
            email: Yup.string().required("Email is required").email("email is invalid"),
            password: Yup.string().required("Password is required").min(6, "password must have min 6 characters").max(32, "password must have max 32 characters "),

        }),
        onSubmit: (values) => {
            userService.signIn(values).then((res) => {
                console.log(res.data.content);
                dispatch(setUserInfor(res.data.content))
                localStorageService.set(res.data.content)
                message.success("sign in successfully")
                setTimeout(() => {
                    window.location.href = `${PROJECT_MANAGEMENT}`
                }, 1000)
            }).catch(err => {
                console.log(err);
                message.error(err.response.data.message)
            })
        },
    })
    return (
        <div className='flex'>
            <div className='w-[40%]'>
                <Lottie animationData={bg_animation} className="h-full w-full" />
            </div>
            <div className='w-[60%] my-auto flex flex-col items-start sm:pl-20 md:pl-40 lg:pl-80'>
                <h1 className='text-center mb-10 text-4xl font-bold'>Sign In Page</h1>
                <form className='mb-5 w-[50%]' onSubmit={formik.handleSubmit}>
                    <div className='text-left'>
                        <Input
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            type="email"
                            placeholder='email'
                            name="email" className='w-full min-w-[300px]' />
                        {formik.errors.email && formik.touched.email ? <span className='text-red-600'>{formik.errors.email}</span> : <></>}
                    </div>

                    <div className='text-left'>
                        <Input
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            type="password"
                            name="password"
                            placeholder='password' className='w-full min-w-[300px] mt-5' />
                        {formik.errors.password && formik.touched.password ? <span className='text-red-600'>{formik.errors.password}</span> : <></>}
                    </div>

                    <div className='mt-5 flex justify-start'>
                        <button type='submit' className='px-3 py-1 border-2 rounded-md'>Sign In</button>
                    </div>
                </form>
                <div className='mb-5'>
                    <span className='mr-2'>Don't have account?</span>
                    <span className='text-blue-600 underline' onClick={() => { navigate(`${SIGN_UP}`) }}>Sign Up</span>
                </div>
                <div className='flex justify-center text-4xl space-x-5'>
                    <span><FaFacebookSquare /></span>
                    <span><FaGooglePlusSquare /></span>
                    <span><FaGithubSquare /></span>
                    <span><FaGitlab /></span>
                </div>
            </div>
        </div >
    );
}

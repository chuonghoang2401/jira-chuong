import React, { useRef, useState } from 'react'
import { useSpring, animated } from 'react-spring'
import "./DemoDragDrop.css"
// import { useSpring, animated } from "react-spring"

const defaultValue = [
    { id: 1, taskName: "task 1" },
    { id: 2, taskName: "task 2" },
    { id: 3, taskName: "task 3" },
    { id: 4, taskName: "task 4" },
    { id: 5, taskName: "task 5" },
]

export default function DemoDragDrop(props) {
    const [taskList, setTaskList] = useState(defaultValue)
    const tagDrag = useRef({})
    const tagDragEnter = useRef({})
    //Animation
    const [propsSpring, set, stop] = useSpring(() => ({ from: { buttom: -25 }, to: { buttom: 0 }, config: { duration: 8000 }, reset: true }))
    const handleDragStart = (e, task, index) => {
        tagDrag.current = task

    }
    const handleDragOver = (e) => {
        // console.log("target over: ", e.target);
    }
    const handleDropEnd = (e) => {
        // tagDrag.current = {}
        // setTaskList([...taskList])
    }

    const handleDrop = (e) => {
        console.log("drag: ", e.target);
    }

    const handleDragEnter = (e, taskDragEnter, index) => {
        // console.log("dragEnterTag: ", e.target);
        // console.log("targetOver: ", taskDragEnter);
        // console.log("index: ", index);
        //lưu lại giá trị của task được kéo ngang qua
        set({ bottom: 0 })
        tagDragEnter.current = { ...taskDragEnter }
        let taskListUpdate = [...taskList]
        //Lấy index của tag gốc (tag được kéo qua)
        let indexTagDrag = taskListUpdate.findIndex(task => task.id === tagDrag.current.id)
        //Lấy index của tag mục tiêu (target tag) 
        let indexTagDragEnter = taskListUpdate.findIndex(task => taskDragEnter.id === task.id)
        //swap 2 tag lại
        let temporaryTag = taskListUpdate[indexTagDrag]
        taskListUpdate[indexTagDrag] = taskListUpdate[indexTagDragEnter]
        taskListUpdate[indexTagDragEnter] = temporaryTag
        setTaskList(taskListUpdate)
    }
    return (
        <div
            onDragOver={(e) => { e.preventDefault(); e.stopPropagation() }}
            onDrop={(e) => {
                tagDrag.current = {}
                setTaskList([...taskList])
            }}
        >
            <div className='text-center text-2xl'>Task List</div>
            <div className='grid grid-cols-3'>
                <div></div>
                <div className='bg-neutral-700 p-5'>
                    {taskList.map((task, index) => {

                        let cssDragTag = task.id === tagDrag.current.id ? "dragTag" : ""

                        if (task.id === tagDragEnter.current.id) {
                            return (
                                <animated.div>
                                    <div
                                        style={{
                                            position: "relative", bottom: propsSpring.buttom.interpolate(numBottm => `${numBottm}px`)
                                        }}
                                        onDragStart={(e) => { handleDragStart(e, task, index) }}
                                        onDragEnter={(e) => { handleDragEnter(e, task, index) }}
                                        onDragEnd={(e) => { handleDropEnd(e) }}
                                        draggable="true"
                                        className={`bg-green-600 text-white m-1 p-3 ${cssDragTag}`}>{task.taskName}
                                    </div>
                                </animated.div>
                            )
                        }
                        return (
                            <div
                                onDragStart={(e) => { handleDragStart(e, task, index) }}
                                onDragEnter={(e) => { handleDragEnter(e, task, index) }}
                                onDragEnd={(e) => { handleDropEnd(e) }}
                                draggable="true"

                                className={`bg-green-600 text-white m-1 p-3 ${cssDragTag}`}>{task.taskName}</div>
                        )
                    })}
                </div>
                <div className='bg-gray-600'
                    onDragOver={(e) => {
                        e.stopPropagation()
                        e.preventDefault()
                    }}
                    onDrop={(e) => { handleDrop(e) }}></div>
            </div>

        </div >
    )
}

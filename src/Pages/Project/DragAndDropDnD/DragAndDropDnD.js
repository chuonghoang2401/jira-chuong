import React, { useState } from 'react'
import _ from "lodash"
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'

export default function DragAndDropDnD(props) {
    const [state, setState] = useState({
        backLog: {
            id: "backLog",
            items: [
                { id: "1", taskName: "Task 01" },
                { id: "2", taskName: "Task 02" },
                { id: "3", taskName: "Task 03" },
                { id: "4", taskName: "Task 04" }
            ]
        },
        todo: {
            id: "todo",
            items: [
                { id: "5", taskName: "Task 05" },
                { id: "6", taskName: "Task 06" },
                { id: "7", taskName: "Task 07" },
                { id: "8", taskName: "Task 08" }
            ]
        },
        inProgress: {
            id: "inProgress",
            items: [
                { id: "9", taskName: "Task 09" },
                { id: "10", taskName: "Task 10" },
                { id: "11", taskName: "Task 11" },
                { id: "12", taskName: "Task 12" }
            ]
        },
        done: {
            id: "done",
            items: [
                { id: "13", taskName: "Task 13" },
                { id: "14", taskName: "Task 14" },
                { id: "15", taskName: "Task 15" },
                { id: "16", taskName: "Task 16" }
            ]
        },
    })

    const handleDragEnd = (result) => {
        const { source, destination } = result
        console.log("source: ", source);
        console.log("destination: ", destination);

        if (!destination) {
            return
        }

        if (destination.droppableId === source.droppableId && destination.index === source.index) {
            return
        }
        let itemCopy = { ...state[source.droppableId].items[source.index] }
        state[source.droppableId].items.splice(source.index, 1)
        state[destination.droppableId].items.splice(destination.index, 0, itemCopy)
        setState(state)
    }
    return (
        <div>
            <h3>Demo Drag Drop DnD</h3>
            <div>
                <DragDropContext onDragEnd={handleDragEnd}>
                    <div className='grid grid-cols-4 gap-5'>
                        {_.map(state, (statusTask, index) => {
                            return (
                                <Droppable droppableId={statusTask.id} key={index}>
                                    {(provided) => {
                                        return (
                                            <div>
                                                <div className='bg-neutral-800 text-white text-center p-5'
                                                    key={index}
                                                    ref={provided.innerRef}
                                                    {...provided.droppableProps}
                                                >
                                                    <h2>{statusTask.id}</h2>
                                                    {statusTask.items.map((task, index) => {
                                                        return (
                                                            <Draggable draggableId={task.id} key={task.id} index={index}>
                                                                {(provided) => {
                                                                    return (
                                                                        <div className='p-5 bg-green-600 text-white mt-2'
                                                                            ref={provided.innerRef}
                                                                            {...provided.draggableProps}
                                                                            {...provided.dragHandleProps}
                                                                        >
                                                                            <p>{task.taskName}</p>
                                                                        </div>
                                                                    )
                                                                }}

                                                            </Draggable>
                                                        )
                                                    })}
                                                    {console.log("placeholder: ", provided.placeholder.props.shouldAnimate, " index: ", index)}
                                                    {provided.placeholder}
                                                </div>

                                            </div>
                                        )

                                    }}
                                </Droppable>
                            )

                        })}
                    </div>

                </DragDropContext>
            </div >
        </div >
    )
}

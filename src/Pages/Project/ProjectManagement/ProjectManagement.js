import React, { useEffect, useRef, useState } from 'react';
import { Button, message, Popconfirm, Popover, Table, Tag } from 'antd';
import { projectService } from '../../../services/projectService';
import { BsGear, BsPencilSquare, BsTrash } from "react-icons/bs"
import { useDispatch } from 'react-redux';
import { openEditModal, setCallBackFetchProjectList, setEditProject } from '../../../redux/projectReducer/projectReducer';
import { openNotification } from '../../../redux/notificationReducer/notificationReducer';
import { NOTIFICATION_SUCCESS, PROJECT_DASHBOARD } from '../../../utils/constant/constant';
import SearchBar from '../../../Component/SearchBar/SearchBar';
import PopHover from '../../../Component/PopHover/PopHover';
import { NavLink } from 'react-router-dom';
import { openPopup } from '../../../redux/popupReducer/popupReducer';
import EditModal from '../../../Component/FormUpdateProject/EditModal/EditModal';

export default function ProjectManagement() {
    const [data, setData] = useState([])
    const [currentPage, setCurrentPage] = useState(1);
    const dispatch = useDispatch()
    const fetchProjectList = () => {
        projectService.getAllProject().then((res) => {
            let projectList = res.data.content.map((project) => {
                return {
                    ...project, action: (
                        <div className='text-white'>
                            <button className='p-2 bg-blue-600 rounded-md mr-2'
                                onClick={() => handleEditProject(project)}><BsPencilSquare />
                            </button>
                            <Popconfirm
                                placement="bottomRight"
                                title='Are you sure to delete this task?'
                                description='Delete the task'
                                onConfirm={() => confirm(project.id)}
                                okText={<span className='text-neutral-900 hover:text-white'>Delete</span>}
                                cancelText="Cancel"
                            >
                                <button className='p-2 bg-red-600 rounded-md' ><BsTrash /></button>
                            </Popconfirm>
                        </div >
                    )
                }
            })
            console.log("fetch data");
            setData(projectList)

        }).catch((err) => {
            console.log(err);
        })
    }
    const handleEditProject = (editProject) => {
        dispatch(openPopup(<EditModal project={editProject} />))
        dispatch(openEditModal())
        dispatch(setEditProject(editProject))
        dispatch(setCallBackFetchProjectList(fetchProjectList))
    }
    const handleDelete = (projectId) => {
        projectService.deleteProject(projectId).then((res) => {
            dispatch(openNotification({ type: NOTIFICATION_SUCCESS, description: "Deleted successfully" }))
            fetchProjectList()
        }).catch(err => {
            console.log(err);
            message.error("Fail to delete")
        })
    }
    const confirm = (projectId) => {
        handleDelete(projectId)
    };
    useEffect(() => {
        dispatch(setCallBackFetchProjectList(fetchProjectList))
        fetchProjectList()
    }, [])
    return (
        <div>
            <h1 className='font-bold text-2xl mb-5'>Project management</h1>
            <Table columns={columns} dataSource={data}
                pagination={{ current: currentPage, pageSize: 8, onChange: (page) => { setCurrentPage(page) } }}
            />
        </div>
    )
}

const columns = [
    {
        title: 'Id',
        dataIndex: 'id',
        key: 'id',
        render: (text) => <a>{text}</a>,
    },
    {
        title: 'Project Name',
        dataIndex: 'projectName',
        key: 'projectName',
        render: (_, record) => {
            return (
                <NavLink to={`${PROJECT_DASHBOARD}/${record.id}`}>
                    <p>{record.projectName}</p>
                </NavLink>)
        }
    },
    {
        title: 'Category',
        dataIndex: 'categoryName',
        key: 'categoryName',
    },
    {
        title: 'Creator',
        key: 'creator',
        dataIndex: 'creator',
        render: (_, record) => (
            <Tag color="green" key={record.creator.name}>
                {record.creator.name}
            </Tag>
        ),
    },
    {
        title: 'members',
        dataIndex: 'members',
        key: 'members',
        render: (_, record) => {
            return (
                <div className='flex items-center'>
                    <PopHover members={record.members} projectId={record.id} />
                    <Popover placement="rightTop" title={<span>Add user</span>}
                        content={() => { return <SearchBar projectId={record.id} /> }}
                        trigger="click">
                        <button className='bg-neutral-200 w-10 h-10 leading-9 rounded-full text-center border-2'>
                            +
                        </button>
                    </Popover>
                </div>
            )
        }
    },
    {
        title: (<><BsGear /></>),
        dataIndex: "action",
        key: 'action',
    },
];
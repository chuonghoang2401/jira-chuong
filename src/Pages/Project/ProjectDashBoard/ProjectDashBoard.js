import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, useLocation, useParams } from 'react-router-dom';
import { useEffect } from 'react';
import { projectService } from '../../../services/projectService';
import { useState } from 'react';
import DashBoardHeader from './DashBoardHeader/DashBoardHeader';
import DashBoardContent from './DashBoardContent/DashBoardContent';
import { setCallBackFetchProjectDetail, setCallBackFetchProjectList, setProjectDetail } from '../../../redux/projectReducer/projectReducer';


export default function ProjectDashBoard() {
    const project = useSelector(state => state.projectReducer.projectDetail)
    const { projectId } = useParams()
    const dispatch = useDispatch()

    const fetchProjectDetail = () => {
        projectService.getProjectDetail(projectId).then((res) => {
            dispatch(setProjectDetail(res.data.content))
        }).catch((err) => {
            console.log(err);
        })
    }
    useEffect(() => {
        fetchProjectDetail()
        dispatch(setCallBackFetchProjectDetail(fetchProjectDetail))
    }, [])

    return (Object.keys(project).length !== 0
        ?
        <div className='space-y-3'>
            <DashBoardHeader project={project} />
            <DashBoardContent taskLane={project.lstTask} />
        </div>
        :
        <></>)
}

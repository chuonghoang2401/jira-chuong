import React from 'react'
import { useState } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { useSelector } from 'react-redux';

import TaskCard from '../../../../Component/TaskCard/TaskCard'
import { projectService } from '../../../../services/projectService';


export default function DashBoardContent(props) {
    const { callBackFetchProjectDetail } = useSelector(state => state.projectReducer)
    const handleDrageEnd = (result) => {
        const { source, destination } = result
        console.log("source: ", source);
        console.log("destination: ", destination);
        const updateStatus = {
            "taskId": source.index,
            "statusId": destination.droppableId
        }
        projectService.putUpdateStatus(updateStatus).then((res) => {
            console.log(res);
            callBackFetchProjectDetail()
        }).catch((err) => {
            console.log(err);
        })
    }
    return (
        <div>
            <DragDropContext onDragEnd={handleDrageEnd}>
                <div className='grid grid-cols-4 gap-4'>
                    {props.taskLane.map((lane, index) => {

                        return (
                            <Droppable droppableId={lane.statusId.toString()}>
                                {(provided) => {
                                    return (
                                        <div>
                                            <div style={{ backgroundColor: "#f0f2f5", padding: "10px", borderRadius: "10px" }}
                                                ref={provided.innerRef}
                                                {...provided.droppableProps}
                                            >
                                                <h1 className='mb-5'>{lane.statusName}</h1>
                                                <div className='space-y-2'>
                                                    {lane.lstTaskDeTail.map((task, index) => {
                                                        return (
                                                            <Draggable draggableId={task.taskId.toString()}
                                                                index={task.taskId}>
                                                                {(provided) => {
                                                                    return (
                                                                        <div
                                                                            ref={provided.innerRef}
                                                                            {...provided.dragHandleProps}
                                                                            {...provided.draggableProps}
                                                                        >
                                                                            <TaskCard task={task} />
                                                                        </div>

                                                                    )
                                                                }}

                                                            </Draggable>
                                                        )
                                                    })}
                                                </div>
                                            </div>
                                            {provided.placeholder}
                                        </div>

                                    )
                                }}

                            </Droppable>

                        )
                    })}
                </div>
            </DragDropContext>

        </div>
    )
}

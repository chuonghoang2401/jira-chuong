import { http } from "./configURL"

export const taskService = {
    postCreateTask: (task) => {
        return http.post("/api/Project/createTask", task)
    },
    deleteTask: (taskId) => {
        return http.delete(`api/Project/removeTask?taskId=${taskId}`)
    },

    getTaskDetail: (taskId) => {
        return http.get(`api/Project/getTaskDetail?taskId=${taskId}`)
    },
    updateTask: (taskUpdate) => {
        return http.post("api/Project/updateTask", taskUpdate)
    }

}
import { http } from "./configURL"

export const projectService = {
    getAllProject: () => {
        return http.get("/api/Project/getAllProject")
    },
    getProjectDetail: (projectId) => {
        return http.get(`/api/Project/getProjectDetail?id=${projectId}`)
    },
    deleteProject: (projectId) => {
        return http.delete(`/api/Project/deleteProject?projectId=${projectId}`)
    },
    putUpdateProject: (updatingModel) => {
        return http.put(`/api/Project/updateProject?projectId=${updatingModel.id}`, updatingModel)
    },
    postCreateProject: (project) => {
        return http.post("/api/Project/createProject", project)
    },
    getProjectCategory: () => {
        return http.get("/api/ProjectCategory")
    },
    postAssignProjectUser: (projectUser) => {
        return http.post("/api/Project/assignUserProject", projectUser)
    },
    postRemoveUserFromProject: (projectUser) => {
        return http.post("/api/Project/removeUserFromProject", projectUser)
    },
    putUpdateStatus: (updateStatus) => {
        return http.put("/api/Project/updateStatus", updateStatus)
    }



}


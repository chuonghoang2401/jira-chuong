import { http } from "./configURL"

export const priorityService = {
    getAllPriority: () => {
        return http.get("api/Priority/getAll")
    }
}
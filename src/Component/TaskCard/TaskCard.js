import { Card } from 'antd'
import React, { useEffect } from 'react'
import { MdArrowUpward, MdFlag } from "react-icons/md"
import parse from 'html-react-parser';
import { useDispatch } from 'react-redux'
import { openEditTaskModal, setTaskDetail } from '../../redux/taskReducer/taskReducer'
import { openPopup } from '../../redux/popupReducer/popupReducer'
import EditTaskModal from '../FormUpdateTask/EditTaskModal/EditTaskModal'
import { taskService } from '../../services/taskService'
import { useState } from 'react'

import { Avatar, Tooltip } from 'antd';
export default function TaskCard(props) {
    const dispatch = useDispatch()
    const [task, setTask] = useState({})
    const onClick = () => {
        dispatch(openPopup(<EditTaskModal></EditTaskModal>))
        dispatch(openEditTaskModal())
        dispatch(setTaskDetail(task))
    }
    useEffect(() => {
        taskService.getTaskDetail(props.task.taskId).then((res) => {
            //we have to custom array assigness of object newTask
            let newTask = { ...res.data.content }
            let cloneAssignees = newTask.assigness
            newTask.assigness = cloneAssignees.map((user) => {
                user.userId = user.id
                // delete user.id
                return user
            })
            setTask(newTask)

        }).catch((err) => {
            console.log(err);
        })
    }, [props.task])

    return (Object.keys(task).length !== 0 ?
        <Card title={task.taskName} bordered={false} onClick={onClick} className="h-[200px]" draggable="true">
            <div className='grid grid-rows-2'>
                <p className='text-left overflow-y-hidden h-12'>
                    {parse(task.description)}
                </p>
                <div className="flex items-center justify-between">
                    <div className="flex text-2xl">
                        <MdFlag className='text-green-600' />
                        <MdArrowUpward className='text-red-600' />
                    </div>
                    <div className="block-right">
                        <Avatar.Group
                            maxCount={2}
                            maxStyle={{
                                color: '#f56a00',
                                backgroundColor: '#fde3cf',
                            }}
                        >
                            {task.assigness.map((user, index) => {
                                return (
                                    <Tooltip title={user.name} placement="top">
                                        <Avatar
                                            src={user.avatar}
                                        />
                                    </Tooltip>)
                            })}
                        </Avatar.Group>
                    </div>
                </div>
            </div>

        </Card>
        :
        <></>)

}

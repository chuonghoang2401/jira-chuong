import { Button, Modal } from 'antd';
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closePopup } from '../../../redux/popupReducer/popupReducer';
import { openEditModal } from '../../../redux/projectReducer/projectReducer';
import { closeEditTaskModal, openEditTaskModal, setTaskDetail } from '../../../redux/taskReducer/taskReducer';
import FormUpdateTask from '../FormUpdateTask';

export default function EditTaskModal() {
    const isModalOpen = useSelector(state => state.taskReducer.isModalOpen)
    const callTaskCallBackSubmit = useSelector(state => state.taskReducer.taskCallBackSubmit)
    const dispatch = useDispatch()
    const handleOk = () => {
        callTaskCallBackSubmit()
        dispatch(closeEditTaskModal())
    };
    const handleCancel = () => {
        dispatch(setTaskDetail({}))
        dispatch(closeEditTaskModal())
        dispatch(closePopup())
    };
    return (
        <>
            <Modal
                width={600}
                title="Basic Modal"
                open={isModalOpen}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Return
                    </Button>,
                    <Button key="submit" onClick={handleOk}>
                        Update
                    </Button>,
                ]}
            >
                <FormUpdateTask />
            </Modal>
        </>
    );
}

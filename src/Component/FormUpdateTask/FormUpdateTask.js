import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import parse from 'html-react-parser';
import { MdContentCopy, MdFlag, MdOutlineDeleteSweep } from 'react-icons/md';
import { BsTelegram } from 'react-icons/bs';
import a from "../../assets/img/a.jpg"
import { commentService } from '../../services/commentService';
import { Button, Popover, Select, Slider } from 'antd';
import { addMemberToTaskDetail, removeMemberFromTaskDetail, setFieldTaskDetail, setTaskCallBackSubmit } from '../../redux/taskReducer/taskReducer';
import { AiFillClockCircle, AiOutlineDelete } from "react-icons/ai"
import { Editor } from '@tinymce/tinymce-react';
import { store } from "../../redux/store"
import { useFormik } from 'formik';
import *as Yup from "yup"
import { taskService } from '../../services/taskService';
import { openNotification } from '../../redux/notificationReducer/notificationReducer';
import { NOTIFICATION_ERROR, NOTIFICATION_SUCCESS } from '../../utils/constant/constant';


const inputCss = `w-full px-2 py-1.5 bg-white border shadow-sm border-slate-300 
focus:outline-none focus:border-sky-500 focus:ring-sky-500 rounded-md text-sm`
export default function FormUpdateTask() {
    const task = useSelector(state => state.taskReducer.taskDetail)
    const taskTypeList = useSelector(state => state.taskTypeReducer.taskTypeList)
    const statusList = useSelector(state => state.statusReducer.statusList)
    const priorityList = useSelector(state => state.priorityReducer.priorityList)
    const { projectDetail, callBackFetchProjectDetail } = useSelector(state => state.projectReducer)
    const [comments, setComments] = useState([])
    const [visibleEditor, setVisibleEditor] = useState(false)
    const [historycontent, setHistorycontent] = useState(task.description)
    const [content, setContent] = useState(task.description)
    const editorRef = useRef(null);
    const dispatch = useDispatch()
    const formik = useFormik({
        initialValues: {
            listUserAsign: task.assigness,
            taskId: task.taskId,
            taskName: task.taskName,
            description: task.description,
            statusId: task.statusId,
            originalEstimate: task.originalEstimate,
            timeTrackingSpent: task.timeTrackingSpent,
            timeTrackingRemaining: task.timeTrackingRemaining,
            projectId: task.projectId,
            typeId: task.typeId,
            priorityId: task.priorityId,
        },
        validationSchema: Yup.object().shape({}),
        onSubmit: (values) => {
            let updateTask = { ...values }

            updateTask.listUserAsign = values.listUserAsign.map((user => {
                return user.userId
            }))


            taskService.updateTask(updateTask).then((res) => {
                console.log(res.data.content);
                callBackFetchProjectDetail()
                dispatch(openNotification({ type: NOTIFICATION_SUCCESS, description: "Updated task successfully" }))
            }).catch((err) => {
                console.log(err);
                dispatch(openNotification({ type: NOTIFICATION_ERROR, description: err.response.data.content }))
            })
        }

    })
    const renderDescription = () => {
        const jsxDescription = parse(task.description)
        return (
            <>
                <div>Description</div>
                <div>
                    {visibleEditor
                        ?
                        <div>
                            <Editor
                                apiKey='tcvcx7qqseyscb3adpxwafgjzi507hmvgz2e4823v9jokv5u'
                                onInit={(evt, editor) => editorRef.current = editor}
                                initialValue={formik.values.description}
                                onEditorChange={(content, editor) => {
                                    //we don't immediately dispatch (dispatch(setFieldTaskDetail({ name: "description", value: content })))
                                    // as user may choose to cancle option ,so we have to setContent. and Save it when user press save btn
                                    setContent(content)
                                    console.log(content);
                                    formik.setFieldValue("description", content)
                                }}
                                name="description"
                                init={{
                                    height: 300,
                                    menubar: false,
                                    plugins: [
                                        'advlist autolink lists link image charmap print preview anchor',
                                        'searchreplace visualblocks code fullscreen',
                                        'insertdatetime media table paste code help wordcount'
                                    ],
                                    toolbar: 'undo redo | formatselect | ' +
                                        'bold italic backcolor | alignleft aligncenter ' +
                                        'alignright alignjustify | bullist numlist outdent indent | ' +
                                        'removeformat | help',
                                    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
                                }}

                            />
                            <Button onClick={(e) => {
                                dispatch(setFieldTaskDetail({ name: "description", value: content }))
                                setVisibleEditor(false)
                            }}>Save</Button>
                            <Button onClick={() => {
                                dispatch(setFieldTaskDetail({ name: "description", value: historycontent }))
                                setVisibleEditor(false)
                            }}>Close</Button>
                        </div>
                        :
                        <div onClick={() => {
                            setHistorycontent(content)
                            setVisibleEditor(true)
                        }}>{jsxDescription}</div>
                    }
                </div>
            </>
        )
    }

    useEffect(() => {
        commentService.getAllComment(task.taskId).then((res) => {
            setComments(res.data.content)
        }).catch((err) => {
            console.log(err);
        })
        dispatch(setTaskCallBackSubmit(formik.handleSubmit))
    }, [])

    return (
        <div>
            <form onSubmit={formik.handleSubmit}>
                <div className='modal-header'>
                    {renderHeader(taskTypeList, formik, callBackFetchProjectDetail)}
                </div>
                <div className='modal-body mt-5'>
                    <div className="container-fluid">
                        <div className="grid grid-cols-12 text-left gap-8">
                            <div className="col-span-7 space-y-3 text-base">
                                <div className="description">
                                    {renderDescription()}
                                </div>
                                <div className="comment">
                                    {renderComment(comments)}
                                </div>
                            </div>

                            <div className="col-span-5 text-xs space-y-3">
                                <div className="status">
                                    {renderStatus(statusList, formik)}
                                </div>

                                <div className="assignees">
                                    {renderAssignees(task, projectDetail, formik)}
                                </div>

                                <div className="priority">
                                    {renderPriority(priorityList, formik)}
                                </div>

                                <div className="estimate">
                                    {renderOriginalEstimate(formik)}
                                </div>

                                <div className="time-tracking">
                                    {renderTimeTracking(formik)}
                                </div>
                                <div style={{ color: '#929398' }}>Create at a month ago</div>
                                <div style={{ color: '#929398' }}>Update at a few seconds ago</div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>

        </div >
    )
}

const handleDeleteTask = (taskId, callBackFetchProjectDetail) => {
    taskService.deleteTask(taskId).then((res) => {
        console.log(res.data.content);
        callBackFetchProjectDetail()
        store.dispatch(openNotification({ type: NOTIFICATION_SUCCESS, description: "deleted successfully" }))
    }).catch((err) => {
        console.log(err);
        store.dispatch(openNotification({ type: NOTIFICATION_ERROR, description: "fail to delete" }))
    })
}

const renderHeader = (taskTypeList, formik, callBackFetchProjectDetail) => {
    return (
        <>
            <div className='flex justify-between text-base'>
                <div className="flex items-center">
                    <MdFlag className='text-green-600 text-2xl' />
                    <Select
                        labelInValue
                        defaultValue={{
                            value: taskTypeList[0].id,
                            label: taskTypeList[0].taskType,
                        }}
                        name="typeId"
                        style={{
                            width: 120,
                        }}
                        onChange={(value) => {
                            console.log(value);
                            formik.setFieldValue("typeId", value.value)
                        }}
                        options={
                            taskTypeList.map((type) => {
                                return {
                                    value: type.id,
                                    label: type.taskType,
                                }
                            })
                        }
                    />
                </div>
                <div className='flex items-center space-x-2'>
                    <div className='flex items-center space-x-1'>
                        <span className='text-2xl'><BsTelegram /></span>
                        <span>Give feedback</span>
                    </div>
                    <div className='flex items-center space-x-1'>
                        <span className='text-2xl'><MdContentCopy /></span>
                        <span>Copy link</span>
                    </div>

                    <button type="button" className='text-2xl' onClick={() => { handleDeleteTask(formik.values.taskId, callBackFetchProjectDetail) }}>
                        <MdOutlineDeleteSweep />
                    </button>
                </div>
            </div>
        </>
    )
}
const renderAssignees = (task, projectDetail, formik) => {
    return (
        <>
            <h6 className='font-semibold'>ASSIGNEES</h6>
            <div className='space-y-2'>
                <div className='grid grid-cols-2 gap-2'>
                    {task.assigness.map((user, index) => {
                        let content = (
                            <div className='flex items-center space-x-2 text-sm'>
                                <img src={user.avatar} className="w-8 h-8 rounded-full" alt="" />
                                <span> {user.name}</span>
                            </div>)
                        return (
                            <div className="flex items-center justify-between bg-gray-300 p-1 rounded-md" key={index}>
                                <Popover content={content}>
                                    <div className='flex items-center space-x-1'>
                                        <img src={user.avatar} className="w-7 h-7 rounded-full" alt="" />
                                        <span className='overflow-hidden'>{user.name.length > 5 ? user.name.slice(0, 5) + "..." : user.name}</span>
                                    </div>
                                </Popover>
                                <button onClick={() => {
                                    let member = projectDetail.members.find(mem => mem.userId === user.userId)
                                    let cloneMembers = [...task.assigness]
                                    cloneMembers = cloneMembers.filter((mem) => { return mem.userId !== member.userId })
                                    formik.setFieldValue("listUserAsign", cloneMembers)
                                    store.dispatch(removeMemberFromTaskDetail(user))
                                }}>
                                    <AiOutlineDelete className='text-lg' />
                                </button>
                            </div>
                        )

                    })}
                </div>

                <div>
                    <Select
                        labelInValue
                        defaultValue={{
                            label: '+ Add more',
                        }}
                        name="listUserAsign"
                        style={{
                            width: "100%",
                        }}
                        onChange={(value) => {
                            let member = projectDetail.members.find(mem => mem.userId === Number(value.value))
                            let cloneMembers = [...task.assigness]
                            cloneMembers.push(member)
                            formik.setFieldValue("listUserAsign", cloneMembers)
                            store.dispatch(addMemberToTaskDetail(member))
                        }}
                        options={
                            projectDetail.members.filter((mem) => {
                                let index = task.assigness?.findIndex(user => user.userId === mem.userId)
                                if (index !== -1) { return false }
                                return true
                            }).map((m) => {
                                return { value: m.userId, label: m.name }
                            })
                        }
                    />
                </div>
            </div>

        </>
    )
}
const renderTimeTracking = (formik) => {
    const max = Number(formik.values.timeTrackingSpent) + Number(formik.values.timeTrackingRemaining)
    return (
        <div>
            <h6>TIME TRACKING</h6>
            <div>
                <div className='flex items-center'>
                    <AiFillClockCircle className='text-2xl mr-2' />
                    <Slider
                        className='w-full'
                        min={1}
                        max={max}
                        name="timeTrackingSpent"
                        value={formik.values.timeTrackingSpent}
                        onChange={(values) => {
                            // const { name, value } = e.target
                            // store.dispatch(setFieldTaskDetail({ name, value }))
                            formik.setFieldValue("timeTrackingSpent", values)
                        }}
                    />
                </div>
                <div className='flex justify-between'>
                    <p className='text-xs'>{formik.values.timeTrackingSpent} logged</p>
                    <p className='text-xs'>{max} estimated</p>
                </div>

            </div>
            <div className='grid grid-cols-2 gap-5 mt-5'>
                <div>
                    <label>
                        <strong className='text-xs'>Time spent</strong>
                        <input
                            type="number"
                            name='timeTrackingSpent'
                            onChange={(e) => {
                                formik.handleChange(e)
                            }}
                            value={formik.values.timeTrackingSpent}
                            className={inputCss}
                        />
                    </label>
                </div>
                <div>
                    <label className='overflow-hidden'>
                        <strong className='text-xs'>Time remaining</strong>
                        <input
                            type="number"
                            name='timeTrackingRemaining'
                            onChange={(e) => {
                                formik.handleChange(e)
                            }}
                            className={inputCss}
                            value={formik.values.timeTrackingRemaining}
                        />
                    </label>
                </div>
            </div>
        </div >
    )
}
const renderPriority = (priorityList, formik) => {
    return (
        <>
            <h6 className='font-semibold'>PRIORITY</h6>
            <select
                className='w-full px-2 py-1.5 bg-white border shadow-sm border-slate-300 
                                            focus:outline-none focus:border-sky-500 focus:ring-sky-500 rounded-md text-sm'
                value={formik.values.priorityId}
                name="priorityId"
                onChange={(e) => {
                    // const { name, value } = e.target
                    // store.dispatch(setFieldTaskDetail({ name, value }))
                    formik.handleChange(e)
                }}
            >
                {priorityList.map((priority) => {
                    return (
                        <option value={priority.priorityId}>
                            {priority.priority}
                        </option>
                    )
                })}
            </select>
        </>
    )
}
const renderStatus = (statusList, formik) => {
    return (
        <>
            <h6 className='font-semibold'>STATUS</h6>
            <select
                className={inputCss}
                value={formik.values.statusId}
                name="statusId"
                onChange={(e) => {
                    // const { name, value } = e.target
                    // store.dispatch(setFieldTaskDetail({ name, value }))
                    formik.handleChange(e);
                    // console.log(e);
                }}
            >
                {statusList.map((status) => {
                    return (
                        <option value={status.statusId}>{status.statusName}</option>
                    )
                })}
            </select>
        </>
    )
}
const renderComment = (comments) => {

    return (
        <>
            <h6 className='font-bold mb-3'>Comment</h6>
            <div className="block-comment flex" >
                <div className="avatar">
                    <img src={a} className="w-10 h-10" alt="" />
                </div>
                <div className="input-comment w-80">
                    <input type="text"
                        className='w-full px-2 py-1.5 bg-white border shadow-sm border-slate-300 
                                            focus:outline-none focus:border-sky-500 focus:ring-sky-500 rounded-md text-sm'
                        placeholder="Add a comment ..." />
                    <p>
                        <span className='text-gray-400 font-semibold'>Protip: </span>
                        Press
                        <span style={{ fontWeight: 'bold', background: '#ecedf0', color: '#b4bac6' }}>M</span>
                        to comment
                    </p>
                </div>
            </div>
            <div className='space-y-2'>
                {comments.map((cmt) => {
                    return (
                        <div className='flex items-center space-x-2'>
                            <div>
                                <img src={cmt.user.avatar} className="w-8 h-8 rounded-full" alt="" />
                            </div>
                            <div>
                                <p>{parse(cmt.contentComment)}</p>
                            </div>
                        </div>
                    )
                })}
            </div>
        </>
    )
}
const renderOriginalEstimate = (formik) => {
    return (
        <>
            <h6 className='font-semibold'>ORIGINAL ESTIMATE (HOURS)</h6>
            <input
                type="text" className='w-full px-2 py-1.5 bg-white border shadow-sm border-slate-300 
                                            focus:outline-none focus:border-sky-500 focus:ring-sky-500 rounded-md text-sm'
                name='originalEstimate'
                value={formik.values.originalEstimate}
                onChange={(e) => {
                    // const { name, value } = e.target
                    // store.dispatch(setFieldTaskDetail({ name, value }))
                    formik.handleChange(e)
                }}

            />
        </>
    )
}


import React from 'react'
import { Avatar, Popconfirm, Popover, Tooltip } from 'antd';
import { Table } from 'antd';
import { useEffect } from 'react';
import { useState } from 'react';
import { projectService } from '../../services/projectService';
import { useDispatch, useSelector } from 'react-redux';
import { openNotification } from '../../redux/notificationReducer/notificationReducer';
import { NOTIFICATION_ERROR, NOTIFICATION_SUCCESS, PROJECT_MANAGEMENT } from '../../utils/constant/constant';
import { MdDeleteForever } from "react-icons/md"
import { QuestionCircleOutlined } from '@ant-design/icons';
import { GiAnt } from 'react-icons/gi';
export default function PopHover(props) {
    const { callBackFetchProjectList, callBackFetchProjectDetail } = useSelector(state => state.projectReducer)
    const [members, setMembers] = useState([])
    const dispatch = useDispatch()
    const handleDeleteUser = (userId) => {
        let projectUser = { projectId: props.projectId, userId: userId }
        console.log(projectUser);
        projectService.postRemoveUserFromProject(projectUser).then((res) => {
            console.log(res.data);
            callBackFetchProjectList()
            callBackFetchProjectDetail()
            dispatch(openNotification({ type: NOTIFICATION_SUCCESS, description: "Deleted member successfully" }))
        }).catch((err) => {
            console.log(err);
            dispatch(openNotification({ type: NOTIFICATION_ERROR, description: "Fail to delete member" }))
        })
    };

    const handleAddActionProp = (members) => {
        return members.map((member) => {
            return {
                ...member, action: (
                    <Popconfirm
                        title="Delete the task"
                        description="Are you sure to delete this task?"
                        icon={<QuestionCircleOutlined style={{ color: 'red', }} />}
                        okButtonProps={{ type: "default" }}
                        okText="Delete"
                        onConfirm={() => { handleDeleteUser(member.userId) }}
                    >
                        <button className='p-2 bg-red-600 rounded-md mr-2 text-white text-xl'>
                            <MdDeleteForever />
                        </button>
                    </Popconfirm >

                )
            }
        })
    }

    const content = (<Table columns={columns} dataSource={members} pagination={{ pageSize: 5 }} className="h-[450px]" />);

    useEffect(() => {
        let newArr = handleAddActionProp(props.members)
        setMembers(newArr)
    }, [props.members])
    return (
        <>
            {
                members.length > 0
                    ?
                    < Popover placement='top' content={content} title="Member" className='flex items-center' >
                        {
                            members.slice(0, 1).map((mem, index) => {
                                return (
                                    <Tooltip title={mem.name} placement="top">
                                        <Avatar src={mem.avatar} alt={<GiAnt />} size="large" key={index} />
                                    </Tooltip>
                                );
                            })
                        }
                        <Avatar.Group
                            maxCount={1}
                            maxStyle={{
                                color: '#f56a00',
                                backgroundColor: '#fde3cf',
                            }}
                            size="large"
                        >
                            {
                                members.slice(1, props.members.length).map((mem, index) => {
                                    return (
                                        <Tooltip title={mem.name} placement="top">
                                            <Avatar src={mem.avatar} alt={<GiAnt />} size="large" key={index} />
                                        </Tooltip>
                                    );
                                })
                            }
                        </Avatar.Group>
                    </Popover >
                    :
                    <></>
            }
        </>
    )



}


const columns = [
    {
        title: 'Id',
        dataIndex: 'userId',
        key: 'userId',
    },

    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Avatar',
        dataIndex: 'avatar',
        key: 'avatar',
        render: (text) => (
            <>
                <img src={text} className="w-8 h-8 rounded-full" alt="" />
            </>

        )
    },

    {
        title: 'Action',
        dataIndex: "action",
        key: 'action',
    },
];

import { Select } from 'antd';
import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { openNotification } from '../../redux/notificationReducer/notificationReducer';
import { store } from '../../redux/store';
import { projectService } from '../../services/projectService';
import { userService } from '../../services/userService';
import { NOTIFICATION_ERROR, NOTIFICATION_SUCCESS } from '../../utils/constant/constant';

export default function SearchInput() {
    let timeout;
    const { projectDetail, callBackFetchProjectDetail, callBackFetchProjectList } = useSelector(state => state.projectReducer)
    const [options, setOptions] = useState([]);
    const handleSearch = (value) => {
        if (value) {
            fetch(value, setOptions, timeout);
        } else {
            setOptions([]);
        }
    };
    const handleChange = (value) => {
        let projectUser = { userId: value, projectId: projectDetail.id }
        projectService.postAssignProjectUser(projectUser).then((res) => {
            store.dispatch(openNotification({ type: NOTIFICATION_SUCCESS, description: "Add member successfully" }))
            callBackFetchProjectList()
            callBackFetchProjectDetail()
        }).catch((err) => {
            console.log(err);
            store.dispatch(openNotification({ type: NOTIFICATION_ERROR, description: err.response.data.content }))
        })
    };

    return (
        <Select
            optionFilterProp="label"
            showSearch
            placeholder={<p className='text-left'>Enter username</p>}
            style={{ width: 200, }}
            showArrow={false}
            filterOption={false}
            onSearch={handleSearch}
            onChange={handleChange}
            options={options}
        />
    );
}


const fetch = (value, setOptions, timeout) => {
    let result = []
    if (timeout) {
        clearTimeout(timeout);
        timeout = null;
    }
    const fake = () => {
        userService.getUserWithCondition(value).then((res) => {
            result = res.data.content.map((mem) => ({
                value: mem.userId,
                label: (
                    <div className='flex justify-between items-center'>
                        {mem.name.length <= 20 ? <p>{mem.name}</p> : <p>{mem.name.slice(0, 20)}...</p>}
                        <img src={mem.avatar} className="w-5 h-5 rounded-full" alt="" />
                    </div>

                )
            }))
            setOptions(result);
        }).catch((err) => {
            console.log(err);
        })
    };
    timeout = setTimeout(fake, 300);
};

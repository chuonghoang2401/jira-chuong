import { notification } from 'antd';
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closeNotification } from '../../redux/notificationReducer/notificationReducer';

export default function Notification() {
    const { type, description, enableNotification } = useSelector(state => state.notificationReducer)
    const [api, contextHolder] = notification.useNotification();
    const dispatch = useDispatch()
    const openNotification = (type, description) => {
        api[type]({
            message: `Notification ${type}`,
            description: description,
        });
    };
    useEffect(() => {
        if (enableNotification) {
            openNotification(type, description)
            dispatch(closeNotification())
        }
    }, [enableNotification])

    return (
        <>
            {contextHolder}
        </>
    );
}

import { useFormik } from 'formik';
import React from 'react'
import { useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { localStorageService } from '../../services/localStorageService';
import * as Yup from "yup"
import { projectService } from '../../services/projectService';
import { NOTIFICATION_ERROR, NOTIFICATION_SUCCESS } from '../../utils/constant/constant';
import { openNotification } from '../../redux/notificationReducer/notificationReducer';
import { useEffect } from 'react';
import { setSubmitEditProject } from '../../redux/projectReducer/projectReducer';
import { Input } from 'antd';
import { Editor } from '@tinymce/tinymce-react';

export default function FormUpdateProject() {
    const dispatch = useDispatch()
    const { editProject, callBackFetchProjectList } = useSelector(state => state.projectReducer)
    const category = useSelector(state => state.projectCategoryReducer.projectCategory)
    const editorRef = useRef(null);
    const formik = useFormik({
        initialValues: {
            id: editProject.id,
            projectName: editProject.projectName,
            categoryId: editProject.categoryId,
            description: editProject.description,
            creator: localStorageService.get().id
        },
        validationSchema: Yup.object().shape({
            projectName: Yup.string().required("Project name is required"),
        }),

        onSubmit: (values) => {
            if (editorRef.current) {
                let updatingProject = { ...values, description: editorRef.current.getContent() }
                console.log(updatingProject);
                projectService.putUpdateProject(updatingProject).then((res) => {
                    callBackFetchProjectList()
                    dispatch(openNotification({ type: NOTIFICATION_SUCCESS, description: "Updated successfully" }))
                }).catch((err) => {
                    console.log(err);
                    dispatch(openNotification({ type: NOTIFICATION_ERROR, description: "Failed to update" }))
                })
            }

        },
    })

    useEffect(() => {
        dispatch(setSubmitEditProject(formik.handleSubmit))
        formik.setValues({
            ...formik.values,
            "id": editProject.id,
            "projectName": editProject.projectName,
            "categoryId": editProject.categoryId,
            "description": editProject.description,
        })
    }, [editProject.id])

    return (
        <div>
            <form onSubmit={formik.handleSubmit}>
                <div className='grid grid-cols-3 gap-5'>
                    <div>
                        <label className='space-y-2'>
                            <strong>Project id</strong>
                            <Input
                                readOnly
                                value={formik.values.id}
                                type="text"
                                name="id" />
                        </label>
                    </div>
                    <div>
                        <label className='space-y-2'>
                            <strong>Project name</strong>
                            <Input
                                value={formik.values.projectName}
                                onChange={formik.handleChange}
                                type="text"
                                name="projectName" />
                        </label>
                        {formik.errors.projectName && formik.touched.projectName ? <span className='text-red-600'>{formik.errors.projectName}</span> : <></>}
                    </div>

                    <div>
                        <label className='space-y-2'>
                            <strong>Project Category</strong>
                            <select
                                name="categoryId"
                                onChange={formik.handleChange}
                                value={formik.values.categoryId}
                                className="w-full mt-5 px-2 py-1 bg-white border shadow-sm border-slate-300
                        focus:outline-none focus:border-sky-500 focus:ring-sky-500 rounded-md text-sm"
                            >
                                {category.map((c) => {
                                    return c.id === formik.values.categoryId
                                        ?
                                        <option value={c.id} selected> {c.projectCategoryName}</option>
                                        :
                                        <option value={c.id}> {c.projectCategoryName}</option>

                                })}
                            </select>
                        </label>
                    </div>
                </div>
                <div className='space-y-2 mt-5'>
                    <strong>Description</strong>
                    <Editor
                        apiKey='tcvcx7qqseyscb3adpxwafgjzi507hmvgz2e4823v9jokv5u'
                        onInit={(evt, editor) => editorRef.current = editor}
                        initialValue={editProject.description}
                        init={{
                            height: 300,
                            menubar: false,
                            plugins: [
                                'advlist autolink lists link image charmap print preview anchor',
                                'searchreplace visualblocks code fullscreen',
                                'insertdatetime media table paste code help wordcount'
                            ],
                            toolbar: 'undo redo | formatselect | ' +
                                'bold italic backcolor | alignleft aligncenter ' +
                                'alignright alignjustify | bullist numlist outdent indent | ' +
                                'removeformat | help',
                            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
                        }}
                    />
                </div>
            </form>

        </div>
    )
}

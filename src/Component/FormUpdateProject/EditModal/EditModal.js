import { Button, Modal } from 'antd';
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closePopup } from '../../../redux/popupReducer/popupReducer';
import { closeEditModal, setEditProject } from '../../../redux/projectReducer/projectReducer';
import FormUpdateProject from '../FormUpdateProject';

export default function EditModal() {
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch()
    const { editModalDisplay, callBackSubmit } = useSelector(state => state.projectReducer)
    const handleOk = () => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
            callBackSubmit()
            dispatch(closeEditModal())
            dispatch(setEditProject({}))
        }, 1000);
    };
    const handleCancel = () => {
        dispatch(closeEditModal())
        dispatch(closePopup())
    };
    return (
        <>

            <Modal
                width={600}
                open={editModalDisplay}
                title="Title"
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Return
                    </Button>,
                    <Button key="submit" loading={loading} onClick={handleOk}>
                        Update
                    </Button>,
                ]}
            >
                <FormUpdateProject />
            </Modal>
        </>
    );
}

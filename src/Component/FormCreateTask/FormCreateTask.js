import { Col, Row, Slider, Select, Space } from 'antd'
import { useFormik } from 'formik';
import React, { useRef } from 'react'
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Editor } from '@tinymce/tinymce-react';
import { setUserSearchList } from '../../redux/userReducer/userReducer';
import { userService } from '../../services/userService';
import * as Yup from "yup"
import { taskService } from '../../services/taskService';
import { setTaskCallBackSubmit } from '../../redux/taskReducer/taskReducer';
import { openNotification } from '../../redux/notificationReducer/notificationReducer';
import { NOTIFICATION_ERROR, NOTIFICATION_SUCCESS } from '../../utils/constant/constant';
import { store } from '../../redux/store';

const { Option } = Select;
const inputCss = `w-full px-2 py-1.5 bg-white border shadow-sm border-slate-300 
focus:outline-none focus:border-sky-500 focus:ring-sky-500 rounded-md text-sm`

export default function FormCreateTask() {
    const { projectList, callBackFetchProjectDetail } = useSelector(state => state.projectReducer)
    const taskTypeList = useSelector(state => state.taskTypeReducer.taskTypeList)
    const priorityList = useSelector(state => state.priorityReducer.priorityList)
    const userSearchList = useSelector(state => state.userReducer.userSearchList)
    const statusList = useSelector(state => state.statusReducer.statusList)
    const editorRef = useRef(null);
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            listUserAsign: userSearchList,
            taskName: "",
            description: "",
            statusId: statusList[0]?.statusId,
            originalEstimate: "",
            timeTrackingSpent: 10,
            timeTrackingRemaining: 5,
            projectId: projectList[0]?.id,
            typeId: taskTypeList[0]?.id,
            priorityId: priorityList[0]?.priorityId,

        },
        validationSchema: Yup.object({
            taskName: Yup.string().required("taskName is Required"),
            originalEstimate: Yup.number().required("originalEstimate is required"),
        }),

        onSubmit: (values) => {
            console.log(values);
            if (editorRef.current) {
                let newTask = { ...values, description: editorRef.current.getContent(), }
                newTask.listUserAsign = newTask.listUserAsign.map((user) => { return user.userId })
                console.log(newTask);
                taskService.postCreateTask(newTask).then((res) => {
                    dispatch(openNotification({ type: NOTIFICATION_SUCCESS, description: "Created task successfully" }))
                    callBackFetchProjectDetail()
                    // dispatch(setTaskCallBackSubmit(() => { }))
                }).catch((err) => {
                    console.log(err);
                    dispatch(openNotification({ type: NOTIFICATION_ERROR, description: err.response.data.content }))
                })
            }
        },
    },)

    useEffect(() => {
        dispatch(setTaskCallBackSubmit(formik.handleSubmit))
    }, [])

    return (
        <div>
            <form onSubmit={formik.handleSubmit} className="space-y-3">
                {renderProjectList(projectList, formik)}

                {renderTaskName(formik)}

                {renderStatus(statusList, formik)}
                <div className='grid grid-cols-2 gap-5'>
                    {renderPriority(priorityList, formik)}
                    {renderTaskType(taskTypeList, formik)}
                </div>

                <div className='grid grid-cols-2 gap-5'>
                    {renderAssignees(userSearchList, formik)}
                    <div>
                        <label>
                            <strong>Time Tracking</strong>
                            <Row>
                                <Col span={16}>
                                    <Slider
                                        min={1}
                                        max={Number(formik.values.timeTrackingSpent) + Number(formik.values.timeTrackingRemaining)}
                                        name="timeTrackingSpent"
                                        value={formik.values.timeTrackingSpent}
                                        onChange={(value) => {
                                            formik.setValues({
                                                ...formik.values, timeTrackingSpent: value
                                            })
                                        }}
                                    />
                                </Col>
                                <Col span={8}>
                                    <input type="number"
                                        className={inputCss}
                                        max={Number(formik.values.timeTrackingSpent) + Number(formik.values.timeTrackingRemaining)}
                                        value={Number(formik.values.timeTrackingSpent) + Number(formik.values.timeTrackingRemaining)}
                                        readOnly
                                    />
                                </Col>
                            </Row>
                        </label>
                    </div>
                </div>
                <div className='grid grid-cols-2 gap-5'>
                    {renderOriginalEstimate(formik)}
                    <div className='grid grid-cols-2 gap-5'>
                        {renderTimeTrackingSpent(formik)}
                        {renderTimeTrackingRemaining(formik)}
                    </div>
                </div>
                {renderDescription(editorRef, formik)}
            </form>
        </div>
    )
}

const renderAssignees = (userSearchList, formik) => {
    return (
        <div>
            <label>
                <strong>Assignees</strong>
                <Select
                    mode="multiple"
                    style={{
                        width: '100%',
                    }}
                    name="listUserAsign"
                    placeholder="select one country"
                    optionFilterProp="label"
                    optionLabelProp="label"
                    onChange={(values) => {
                        formik.setFieldValue("listUserAsign", values);
                    }}
                >
                    {userSearchList.map((user) => {
                        return (
                            <Option value={user.userId} label={user.name}>
                                <Space>
                                    <span role="img" aria-label={user.userId}>
                                        {user.name}
                                    </span>
                                </Space>
                            </Option>
                        )
                    })}
                </Select>
            </label>
        </div>
    )
}
const renderTaskType = (taskTypeList, formik) => {
    return (
        <div >
            <label>
                <strong>Task Type</strong>
                <select
                    name="typeId"
                    value={formik.values.typeId}
                    className={inputCss}
                    onChange={formik.handleChange}
                >
                    {taskTypeList.map((type) => {
                        return <option value={type.id}>{type.taskType}</option>
                    })}
                </select>
            </label>
        </div>
    )
}
const renderPriority = (priorityList, formik) => {
    return (
        <div>
            <label>
                <strong>Priority</strong>
                <select
                    name="priorityId"
                    value={formik.values.priorityId}
                    className={inputCss}
                    onChange={formik.handleChange}
                >
                    {priorityList.map((priority) => {
                        return <option value={priority.priorityId}>{priority.priority}</option>
                    })}
                </select>
            </label>
        </div>

    )
}
const renderStatus = (statusList, formik) => {
    return (
        <div>
            <label>
                <strong>Status</strong>
                <select
                    name="statusId"
                    className={inputCss}
                    onChange={formik.handleChange}
                    value={formik.values.statusId}

                >
                    {statusList.map((status) => {
                        return <option value={status.statusId}>{status.statusName}</option>
                    })}
                </select>
            </label>
        </div>
    )
}
const renderTaskName = (formik) => {
    return (
        <div>
            <label>
                <strong>Task Name</strong>
                <input
                    name="taskName"
                    className={inputCss}
                    onChange={formik.handleChange}
                    value={formik.values.taskName}
                />
            </label>
            {formik.errors.taskName && (<p className='text-red-600'>{formik.errors.taskName}</p>)}
        </div>
    )
}
const renderProjectList = (projectList, formik) => {
    return (
        <div>
            <label>
                <strong>Project</strong>
                <select
                    name="projectId"
                    className={inputCss}
                    value={formik.values.projectId}
                    onChange={(e) => {
                        console.log(e.target.value);
                        userService.getUserByProjectId(e.target.value).then((res) => {
                            store.dispatch(setUserSearchList(res.data.content))
                        }).catch((err) => { store.dispatch(setUserSearchList([])) })
                        formik.handleChange(e);
                    }}
                >
                    {projectList.map((project) => {
                        return <option value={project.id}>{project.projectName}</option>
                    })}
                </select>
            </label>
        </div>
    )
}

const renderOriginalEstimate = (formik) => {
    return (
        <div>
            <label>
                <strong>Original Estimate</strong>
                <input
                    type="number"
                    name='originalEstimate'
                    className={inputCss}
                    onChange={formik.handleChange}
                />
            </label>
            {formik.errors.originalEstimate && (<p className='text-red-600'>{formik.errors.originalEstimate}</p>)}
        </div>
    )
}
const renderTimeTrackingSpent = (formik) => {
    return (
        <div>
            <label>
                <strong className='text-xs'>Time spent</strong>
                <input
                    type="number"
                    name='timeTrackingSpent'
                    onChange={(e) => {
                        formik.handleChange(e)
                        formik.setFieldValue("timeTrackingSpent", e.target.value)
                    }}
                    value={formik.values.timeTrackingSpent}
                    className={inputCss}
                />
            </label>
        </div>

    )
}
const renderTimeTrackingRemaining = (formik) => {
    return (
        <div>
            <label className='overflow-hidden'>
                <strong className='text-xs'>Time remaining</strong>
                <input
                    type="number"
                    name='timeTrackingRemaining'
                    onChange={(e) => {
                        formik.handleChange(e)
                        formik.setFieldValue("timeTrackingRemaining", e.target.value)
                    }}
                    className={inputCss}
                    value={formik.values.timeTrackingRemaining}
                />
            </label>
        </div>
    )
}

const renderDescription = (editorRef, formik) => {
    return (
        <div>
            <strong>Description</strong>
            <Editor
                apiKey='tcvcx7qqseyscb3adpxwafgjzi507hmvgz2e4823v9jokv5u'
                onInit={(evt, editor) => editorRef.current = editor}
                initialValue={formik.values.description}
                onChange={formik.handleChange}
                // onEditorChange={(content, editor) => { formik.setFieldValue("description", content) }}
                name="description"
                init={{
                    height: 200,
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table paste code help wordcount'
                    ],
                    toolbar: 'undo redo | formatselect | ' +
                        'bold italic backcolor | alignleft aligncenter ' +
                        'alignright alignjustify | bullist numlist outdent indent | ' +
                        'removeformat | help',
                    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
                }}
            />
        </div>
    )
}
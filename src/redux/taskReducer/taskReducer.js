import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    taskCallBackSubmit: () => { },
    taskList: [],
    taskDetail: {},
    isModalOpen: false
}

export const taskSlice = createSlice({
    name: "taskSlice",
    initialState: initialState,
    reducers: {
        setTaskCallBackSubmit: (state, action) => {
            state.taskCallBackSubmit = action.payload
        },
        openEditTaskModal: (state) => {
            state.isModalOpen = true
        },
        closeEditTaskModal: (state) => {
            state.isModalOpen = false
        },
        setTaskList: (state, action) => {
            state.taskList = action.payload
        },
        setTaskDetail: (state, action) => {
            state.taskDetail = action.payload
        },

        setFieldTaskDetail: (state, action) => {
            console.log(action.payload);
            state.taskDetail = { ...state.taskDetail, [action.payload.name]: action.payload.value }
        },
        addMemberToTaskDetail: (state, action) => {
            let index = state.taskDetail.assigness.findIndex(mem => mem.id === action.payload.userId)
            if (index === -1) {
                state.taskDetail.assigness.push(action.payload)
            }
            else {
                return
            }

        },
        removeMemberFromTaskDetail: (state, action) => {
            let index = state.taskDetail.assigness.findIndex(mem => mem.userId === action.payload.userId)
            if (index !== -1) {
                state.taskDetail.assigness.splice(index, 1)
            }
        }

    }
})

export const {
    setTaskCallBackSubmit,
    openEditTaskModal,
    closeEditTaskModal,
    setTaskList,
    setTaskDetail,
    setFieldTaskDetail,
    addMemberToTaskDetail,
    removeMemberFromTaskDetail
} = taskSlice.actions
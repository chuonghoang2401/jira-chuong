import { configureStore } from "@reduxjs/toolkit";
import { projectCategorySlice } from "./categoryProjectReducer/categoryProjectReducer";
import { notificationSlice } from "./notificationReducer/notificationReducer";
import { popupSlice } from "./popupReducer/popupReducer";
import { prioritySlice } from "./priorityReducer/priorityReducer";
import { projectSlice } from "./projectReducer/projectReducer";
import { statusSlice } from "./statusReducer/statusReducer";
import { taskSlice } from "./taskReducer/taskReducer";
import { tasktypeSlice } from "./taskTypeReducer/taskTypeReducer";
import { userSlice } from "./userReducer/userReducer";



export const store = configureStore({
    reducer: {
        userReducer: userSlice.reducer,
        projectCategoryReducer: projectCategorySlice.reducer,
        projectReducer: projectSlice.reducer,
        notificationReducer: notificationSlice.reducer,
        taskTypeReducer: tasktypeSlice.reducer,
        priorityReducer: prioritySlice.reducer,
        taskReducer: taskSlice.reducer,
        statusReducer: statusSlice.reducer,
        popupReducer: popupSlice.reducer,
    }
})
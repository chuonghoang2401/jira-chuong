import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    type: "",
    description: "",
    enableNotification: false
}

export const notificationSlice = createSlice({
    name: "notificationSlice",
    initialState: initialState,
    reducers: {
        openNotification: (state, action) => {
            state.type = action.payload.type
            state.description = action.payload.description
            state.enableNotification = true
        },
        closeNotification: (state) => {
            state.enableNotification = false
        }
    }
})

export const { closeNotification, openNotification } = notificationSlice.actions





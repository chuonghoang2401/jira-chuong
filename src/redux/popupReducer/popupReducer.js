import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    popup: <></>,
    enablePopUp: false,
}

export const popupSlice = createSlice({
    name: "popupSlice",
    initialState: initialState,
    reducers: {
        openPopup: (state, action) => {
            state.popup = action.payload
            state.enablePopUp = true
        },
        closePopup: (state, action) => {
            state.popup = <></>
            state.enablePopUp = false
        }
    }
})

export const { openPopup, closePopup } = popupSlice.actions

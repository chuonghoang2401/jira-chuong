import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    editModalDisplay: false,
    editProject: {},
    callBackSubmit: () => { console.log("begin"); },
    callBackFetchProjectList: () => { },
    callBackFetchProjectDetail: () => { },
    projectDetail: {},
    projectList: []
}

export const projectSlice = createSlice({
    name: "projectSlice",
    initialState: initialState,
    reducers: {

        closeEditModal: (state) => {
            state.editModalDisplay = false
        },
        openEditModal: (state) => {
            state.editModalDisplay = true
        },
        setEditProject: (state, action) => {
            state.editProject = action.payload
        },
        setSubmitEditProject: (state, action) => {
            state.callBackSubmit = action.payload
        },
        setCallBackFetchProjectList: (state, action) => {
            state.callBackFetchProjectList = action.payload
        },
        setCallBackFetchProjectDetail: (state, action) => {
            state.callBackFetchProjectDetail = action.payload
        },
        setProjectDetail: (state, action) => {
            state.projectDetail = action.payload
        },
        setProjectList: (state, action) => {
            // console.log("setProjectList");
            state.projectList = action.payload
        }
    }
})
export const {
    setEditProject,
    closeEditModal,
    openEditModal,
    setSubmitEditProject,
    setCallBackFetchProjectList,
    setCallBackFetchProjectDetail,
    setProjectDetail,
    setProjectList } = projectSlice.actions

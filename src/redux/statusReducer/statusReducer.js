import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    statusList: []
}


export const statusSlice = createSlice({
    name: "statusSlice",
    initialState: initialState,
    reducers: {
        setStatusList: (state, action) => {
            // console.log("setStatusList");
            state.statusList = action.payload
        }
    }
})

export const { setStatusList } = statusSlice.actions